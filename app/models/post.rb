class Post < ActiveRecord::Base
  has_many :comment, :dependent => :destroy
  validates :title, presence: true
  validates :body, presence: true
  
  #presence: true
end
